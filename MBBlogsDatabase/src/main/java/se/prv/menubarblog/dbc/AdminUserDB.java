package se.prv.menubarblog.dbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import general.reuse.database.pool.JDBCConnectionPool;
import se.prv.menubarblog.domain.AdminUser;

public class AdminUserDB {
   
	public static AdminUser getAdminUser(String user) {
		AdminUser out = null;
		JDBCConnectionPool pool = null;
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
		    pool = Commons.createPool();
		    conn = pool.reserveConnection();
		    stmt = conn.prepareStatement("select id, firstName, lastName, email, active, password from adminuser where userName = ?");
		    stmt.setString(1, user);
		    rs = stmt.executeQuery();
		    while(rs.next()) {
		    	out = new AdminUser();
		    	out.setId(rs.getInt(1)); 
		    	out.setFirstName(rs.getString(2));
		    	out.setLastName(rs.getString(3));
		    	out.setEmail(rs.getString(4));
		    	out.setActive(rs.getBoolean(5));
		    	out.setPassword(rs.getString(6));
		    	out.setUserName(user);
		    }
		} catch(Exception e) {
		    e.printStackTrace();
		} finally {
		    try { if (rs != null) rs.close(); } catch (Exception e) {};
		    try { if (stmt != null) stmt.close(); } catch (Exception e) {};
		    if(conn != null) { pool.releaseConnection(conn); }
		}
		//System.out.println("Hittade "+out+"!");
		return out;
	}
	
	public static boolean createAdminUser(AdminUser au) {
		JDBCConnectionPool pool = null;
		Connection conn = null;
		PreparedStatement stmt = null;
		try {
			pool = Commons.createPool();
		    conn = pool.reserveConnection();
		    stmt = conn.prepareStatement("insert into adminuser (firstName, lastName, email, userName, password, active) values ( ?, ?, ?, ?, ?, ? )");
		    stmt.setString(1, au.getFirstName());
		    stmt.setString(2, au.getLastName());
		    stmt.setString(3, au.getEmail());
		    stmt.setString(4, au.getUserName());
		    stmt.setString(5, au.getPassword());
		    stmt.setBoolean(6, au.isActive());
		    stmt.executeUpdate();
		    conn.commit();
		} catch(Exception e) {
			e.printStackTrace();
		    return false;
		} finally {
		    try { if (stmt != null) stmt.close(); } catch (Exception e) {};
		    if(conn != null) { pool.releaseConnection(conn); }
		}
		return true;
	}

	public static boolean deleteAdminUser(AdminUser user) {
		JDBCConnectionPool pool = null;
		Connection conn = null;
		PreparedStatement stmt = null;
		try {
			pool = Commons.createPool();
		    conn = pool.reserveConnection();
		    stmt = conn.prepareStatement("delete from adminuser where id = ?");
		    stmt.setInt(1, user.getId());
		    stmt.executeUpdate();
		    conn.commit();
		} catch(Exception e) {
			e.printStackTrace();
		    return false;
		} finally {
		    try { if (stmt != null) stmt.close(); } catch (Exception e) {};
		    if(conn != null) { pool.releaseConnection(conn); }
		}
		return true;
	}
	
	public static boolean updateAdminUser(List<AdminUser> users, AdminUser user) {
		JDBCConnectionPool pool = null;
		Connection conn = null;
		PreparedStatement stmt = null;
		try {
			pool = Commons.createPool();
		    conn = pool.reserveConnection();
		    stmt = conn.prepareStatement("update adminuser set firstName = ?, lastName = ?, email = ?, userName = ?, active = ? where id = ?");
		    stmt.setString(1, user.getFirstName());
		    stmt.setString(2, user.getLastName());
		    stmt.setString(3, user.getEmail());
		    stmt.setString(4, user.getUserName());
		    stmt.setBoolean(5, user.isActive());
		    stmt.setInt(6, user.getId());
		    stmt.executeUpdate();
		    conn.commit();
		} catch(Exception e) {
			e.printStackTrace();
		    return false;
		} finally {
		    try { if (stmt != null) stmt.close(); } catch (Exception e) {};
		    if(conn != null) { pool.releaseConnection(conn); }
		}
		return true;
	}

	public static List<AdminUser> selectAllAdminUsers() {
		List<AdminUser> out = new ArrayList<AdminUser>();
		
		JDBCConnectionPool pool = null;
		
		Connection conn = null;
		
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
			pool = Commons.createPool();
		    conn = pool.reserveConnection();
		    stmt = conn.prepareStatement("select id, firstName, lastName, email, userName, active from adminuser");
		    rs = stmt.executeQuery();
		    while(rs.next()) {
		    	AdminUser mbo = new AdminUser();
		    	mbo.setId(rs.getInt(1));
		    	mbo.setFirstName(rs.getString(2));
		    	mbo.setLastName(rs.getString(3));
		    	mbo.setEmail(rs.getString(4));
		    	mbo.setUserName(rs.getString(5));
		    	mbo.setActive(rs.getBoolean(6));
		    	out.add(mbo);
		    }
		} catch(Exception e) {
		    e.printStackTrace();
		} finally {
		    try { if (rs != null) rs.close(); } catch (Exception e) {};
		    try { if (stmt != null) stmt.close(); } catch (Exception e) {};
		    if(conn != null) { pool.releaseConnection(conn); }
		}
		
		return out;
	}

}
