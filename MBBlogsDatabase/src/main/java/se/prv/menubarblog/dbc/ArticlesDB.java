package se.prv.menubarblog.dbc;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import general.reuse.database.pool.JDBCConnectionPool;
import se.prv.menubarblog.domain.ArticleObject;

public class ArticlesDB { 

	public static List<ArticleObject> getAllArticles() { 
		List<ArticleObject> out = new ArrayList<ArticleObject>();
		 
		JDBCConnectionPool pool = null;
		
		Connection conn = null;
		
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
			pool = Commons.createPool();
		    conn = pool.reserveConnection();
		    stmt = conn.prepareStatement("select id, visual, orderby from articles");
		    rs = stmt.executeQuery();
		    while(rs.next()) {
		    	ArticleObject ao = new ArticleObject();
		    	ao.setId(rs.getInt(1));
		    	ao.setVisual(rs.getString(2));
		    	ao.setOrderBy(rs.getInt(3));
		    	out.add(ao);
		    }
		} catch(Exception e) {
		    e.printStackTrace();
		} finally {
		    try { if (rs != null) rs.close(); } catch (Exception e) {};
		    try { if (stmt != null) stmt.close(); } catch (Exception e) {};
		    if(conn != null) { pool.releaseConnection(conn); }
		}
		sortAO(out);
		return out;
	}
	public static void sortAO(List<ArticleObject> array) {
		Collections.sort(array, new Comparator<ArticleObject>() {
		    @Override
		    public int compare(ArticleObject o1, ArticleObject o2) {
		    	try {
			        return o1.getOrderBy() - o2.getOrderBy();
		    	}
		    	catch(Exception e) {
		    		return 0;
		    	}
		    } }
		);
	}


	public static boolean deleteArticleObject(ArticleObject ao) {
		JDBCConnectionPool pool = null;
		Connection conn = null;
		PreparedStatement stmt = null;
		try {
			pool = Commons.createPool();
		    conn = pool.reserveConnection();
		    stmt = conn.prepareStatement("delete from articles where id = ?");
		    stmt.setInt(1, ao.getId());
		    stmt.executeUpdate();
		    conn.commit();
		} catch(Exception e) {
			e.printStackTrace();
		    return false;
		} finally {
		    try { if (stmt != null) stmt.close(); } catch (Exception e) {};
		    if(conn != null) { pool.releaseConnection(conn); }
		}
		return true;
	}


	public static boolean updateArticleObject(ArticleObject ao) {
		JDBCConnectionPool pool = null;
		Connection conn = null;
		PreparedStatement stmt = null;
		try {
			pool = Commons.createPool();
		    conn = pool.reserveConnection();
		    Integer orderBy = ao.getOrderBy();
		    if(orderBy == null) {
			    stmt = conn.prepareStatement("update articles set visual = ? where id = ?");
		    }
		    else {
			    stmt = conn.prepareStatement("update articles set visual = ?, orderby = ? where id = ?");
		    }
		    stmt.setString(1, ao.getVisual());
		    if(orderBy != null) {
			    stmt.setInt(2, ao.getOrderBy());
		    }
		    stmt.setInt(2+((orderBy == null) ? 0 : 1), ao.getId());
		    stmt.executeUpdate();
		    conn.commit();
		} catch(Exception e) {
			e.printStackTrace();
		    return false;
		} finally {
		    try { if (stmt != null) stmt.close(); } catch (Exception e) {};
		    if(conn != null) { pool.releaseConnection(conn); }
		}
		return true;
	}


	public static boolean createArticleObject(ArticleObject ao) {
		JDBCConnectionPool pool = null;
		Connection conn = null;
		PreparedStatement stmt = null;
		try {
			pool = Commons.createPool();
		    conn = pool.reserveConnection();
		    Integer orderBy = ao.getOrderBy();
		    if(orderBy == null) {
			    stmt = conn.prepareStatement("insert into articles (visual) values ( ? )", Statement.RETURN_GENERATED_KEYS);
		    }
		    else {
			    stmt = conn.prepareStatement("insert into articles (visual, orderby) values ( ?, ? )", Statement.RETURN_GENERATED_KEYS);
		    }
		    stmt.setString(1, ao.getVisual());
		    if(orderBy != null) {
			    stmt.setInt(2, orderBy);
		    }
		    int affectedRows = stmt.executeUpdate();
		    //System.out.println("Skrivna rader "+affectedRows);
		    try {
		    	ResultSet generatedKeys = stmt.getGeneratedKeys();
	            generatedKeys.next();
	            ao.setId(generatedKeys.getInt(1));
	        }
		    catch(Exception e) {
		    	//System.out.println("Fel 1");
		    	e.printStackTrace();
		    }
		    conn.commit();
		} catch(Exception e) {
	    	//System.out.println("Fel 2");
			e.printStackTrace();
		    return false;
		} finally {
		    try { if (stmt != null) stmt.close(); } catch (Exception e) {};
		    if(conn != null) { pool.releaseConnection(conn); }
		}
		return true;
	}

	public static ArticleObject getArticleObject(String visual) {
		ArticleObject out = null;
		JDBCConnectionPool pool = null;
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
		    pool = Commons.createPool();
		    conn = pool.reserveConnection();
		    stmt = conn.prepareStatement("select id, orderby from articles where visual = ?");
		    stmt.setString(1, visual);
		    rs = stmt.executeQuery();
		    while(rs.next()) {
		    	out = new ArticleObject();
		    	out.setId(rs.getInt(1));
		    	out.setOrderBy(rs.getInt(2));
		    	out.setVisual(visual);
		    }
		} catch(Exception e) {
		    e.printStackTrace();
		} finally {
		    try { if (rs != null) rs.close(); } catch (Exception e) {};
		    try { if (stmt != null) stmt.close(); } catch (Exception e) {};
		    if(conn != null) { pool.releaseConnection(conn); }
		}
		return out;
	}

	public static boolean setData(ArticleObject ao, byte[] bytes) {
		//System.out.println("ArticlesDB.setData "+new String(bytes)+", id="+ao.getId());
		JDBCConnectionPool pool = null;
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
		    pool = Commons.createPool();
		    conn = pool.reserveConnection();
		    stmt = conn.prepareStatement("update articles set data = ? where id = ?");
		    //Blob blob = new Blob();
		    //blob.setBytes(0, bytes);
		    //stmt.setBytes(1,  bytes);
		    stmt.setBinaryStream(1, new ByteArrayInputStream(bytes != null ? bytes : new byte[0]), (bytes != null ? bytes.length : 0));
		    stmt.setInt(2, ao.getId());
		    int count = stmt.executeUpdate();
		    conn.commit();
		    //System.out.println("ArticlesDB.setData, count="+count);
		} catch(Exception e) {
		    e.printStackTrace();
		    return false;
		} finally {
		    try { if (rs != null) rs.close(); } catch (Exception e) {};
		    try { if (stmt != null) stmt.close(); } catch (Exception e) {};
		    if(conn != null) { pool.releaseConnection(conn); }
		}
		return true;
	}

	public static byte[] getData(ArticleObject ao) {
		JDBCConnectionPool pool = null;
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
		    pool = Commons.createPool();
		    conn = pool.reserveConnection();
		    stmt = conn.prepareStatement("select data from articles where id = ?");
		    stmt.setInt(1, ao.getId());
		    rs = stmt.executeQuery();
		    rs.next();
		    //Blob blob = rs.getBlob(1); // creates the blob object from the result
		    /*
		       blob index starts with 1 instead of 0, and starting index must be (long).
		       we want all the bytes back, so this grabs everything.
		       keep in mind, if the blob is longer than max int length, this won't work right
		       because a byte array has max length of max int length.
		    */
		    //byte[] theBytes = blob.getBytes(1L, (int)blob.length());

		    return getByteArrayFromStream(rs.getBinaryStream(1));
		} catch(Exception e) {
		    e.printStackTrace();
		    return null;
		} finally {
		    try { if (rs != null) rs.close(); } catch (Exception e) {};
		    try { if (stmt != null) stmt.close(); } catch (Exception e) {};
		    if(conn != null) { pool.releaseConnection(conn); }
		}
	}
	
	public static byte[] getByteArrayFromStream(InputStream is) throws IOException {
		if (is == null) {
			return new byte[0];
		}
		ByteArrayOutputStream buffer = new ByteArrayOutputStream();

		int nRead;
		byte[] data = new byte[16384];

		while ((nRead = is.read(data, 0, data.length)) != -1) {
			buffer.write(data, 0, nRead);
		}

		buffer.flush();

		return buffer.toByteArray();
	}
	
	public static void main(String args[]) {
		ArticleObject ao = getArticleObject("Böcker");
		System.out.println("V "+ao.getVisual());
		byte article[] = getData(ao);
		if(article != null) {
			System.out.println("Artikel "+new String(article));
			System.out.println("Len "+article.length);
		}
		System.out.println("Len "+article);
		setData(ao, "En artikel".getBytes());
		article = getData(ao);
		System.out.println("Len "+article);
		if(article != null) {
			System.out.println("Artikel "+new String(article));
			System.out.println("Len "+article.length);
		}
	}


	public static ArticleObject getArticleObject(Integer articleId) {
		ArticleObject out = null;
		JDBCConnectionPool pool = null;
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
		    pool = Commons.createPool();
		    conn = pool.reserveConnection();
		    stmt = conn.prepareStatement("select visual, orderby from articles where id = ?");
		    stmt.setInt(1, articleId);
		    rs = stmt.executeQuery();
		    while(rs.next()) {
		    	out = new ArticleObject();
		    	out.setId(articleId);
		    	out.setVisual(rs.getString(1));
		    	out.setOrderBy(rs.getInt(2));
		    }
		} catch(Exception e) {
		    e.printStackTrace();
		} finally {
		    try { if (rs != null) rs.close(); } catch (Exception e) {};
		    try { if (stmt != null) stmt.close(); } catch (Exception e) {};
		    if(conn != null) { pool.releaseConnection(conn); }
		}
		return out;
	}

}
