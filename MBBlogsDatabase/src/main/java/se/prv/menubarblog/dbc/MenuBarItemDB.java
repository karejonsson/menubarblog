package se.prv.menubarblog.dbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import general.reuse.database.pool.JDBCConnectionPool;
import se.prv.menubarblog.domain.MenuBarItemObject;
import se.prv.menubarblog.domain.MenuBarObject;

public class MenuBarItemDB {

	public static List<MenuBarItemObject> getMenuBarItems(MenuBarObject mbo) {
		return getMenuBarItems(mbo.getId());
	}

	public static List<MenuBarItemObject> getMenuBarItems(int mboId) {
		List<MenuBarItemObject> out = new ArrayList<MenuBarItemObject>();
		
		JDBCConnectionPool pool = null;
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
		    pool = Commons.createPool();
		    conn = pool.reserveConnection();
		    stmt = conn.prepareStatement("select id, orderby, visual, active, menubarId, articleId from menubaritem where menubarId = ?");
		    stmt.setInt(1, mboId);
		    rs = stmt.executeQuery();
		    while(rs.next()) {
		    	MenuBarItemObject mbo = new MenuBarItemObject();
		    	mbo.setId(rs.getInt(1));
		    	mbo.setOrderBy(rs.getInt(2));
		    	mbo.setVisual(rs.getString(3));
		    	//System.out.println("Visual 2 <"+mbo.getVisual()+">");
		    	mbo.setActive(rs.getBoolean(4));
		    	mbo.setMenubarId(rs.getInt(5));
		    	try {
		    		mbo.setArticleId(rs.getInt(6));
		    	}
		    	catch(Exception e) {
		    		
		    	}
		    	out.add(mbo);
		    } 
		} catch(Exception e) {
		    e.printStackTrace();
		} finally {
		    try { if (rs != null) rs.close(); } catch (Exception e) {};
		    try { if (stmt != null) stmt.close(); } catch (Exception e) {};
		    if(conn != null) { pool.releaseConnection(conn); }
		}
		//System.out.println("Hittade "+out.size()+" stycken");
		
		sortMBI(out);

		return out;
	}

	public static void sortMBI(List<MenuBarItemObject> array) {
		Collections.sort(array, new Comparator<MenuBarItemObject>() {
		    @Override
		    public int compare(MenuBarItemObject o1, MenuBarItemObject o2) {
		    	try {
			        return o1.getOrderBy() - o2.getOrderBy();
		    	}
		    	catch(Exception e) {
		    		return 0;
		    	}
		    } }
		);
		
	}

	public static boolean deleteMenuBarItemObject(MenuBarObject parent, MenuBarItemObject mbio) {
		
		JDBCConnectionPool pool = null;
		Connection conn = null;
		PreparedStatement stmt = null;
		try {
			pool = Commons.createPool();
		    conn = pool.reserveConnection();
		    stmt = conn.prepareStatement("delete from menubaritem where id = ?");
		    stmt.setInt(1, mbio.getId());
		    stmt.executeUpdate();
		    conn.commit();
		} catch(Exception e) {
			e.printStackTrace();
		    return false;
		} finally {
		    try { if (stmt != null) stmt.close(); } catch (Exception e) {};
		    if(conn != null) { pool.releaseConnection(conn); }
		}
		parent.removeItem(mbio);
		sortMBI(parent.getItems());
		return true;
	}
	
	public static boolean updateMenuBarItemObject(MenuBarObject parent, MenuBarItemObject mbio) {
		
		JDBCConnectionPool pool = null;
		Connection conn = null;
		PreparedStatement stmt = null;
		try {
			pool = Commons.createPool();
		    conn = pool.reserveConnection();
		    Integer articleId = mbio.getArticleId();

		    if(articleId == null) {
			    stmt = conn.prepareStatement("update menubaritem set visual = ?, active = ?, orderBy = ?, menubarId = ? where id = ?");
		    }
		    else {
			    stmt = conn.prepareStatement("update menubaritem set visual = ?, active = ?, orderBy = ?, menubarId = ?, articleId = ? where id = ?");
		    }

		    stmt.setString(1, mbio.getVisual());
		    stmt.setBoolean(2, mbio.isActive());
		    stmt.setInt(3, mbio.getOrderBy());
		    stmt.setInt(4, mbio.getMenubarId());
		    
		    if(articleId != null) {
		    	stmt.setInt(5, articleId);
		    }

		    stmt.setInt(5+((articleId == null) ? 0 : 1), mbio.getId());

		    stmt.executeUpdate();
		    conn.commit();
		} catch(Exception e) {
			e.printStackTrace();
		    return false;
		} finally {
		    try { if (stmt != null) stmt.close(); } catch (Exception e) {};
		    if(conn != null) { pool.releaseConnection(conn); }
		}
		sortMBI(parent.getItems());
		return true;
	}

	public static boolean createMenuBarItemObject(MenuBarObject parent, MenuBarItemObject mbio) {
		
		JDBCConnectionPool pool = null;
		Connection conn = null;
		PreparedStatement stmt = null;
		try {
			pool = Commons.createPool();
		    conn = pool.reserveConnection();
		    stmt = conn.prepareStatement("insert into menubaritem (visual, active, orderby, menubarId) values ( ?, ?, ?, ? )", Statement.RETURN_GENERATED_KEYS);
		    stmt.setString(1, mbio.getVisual());
		    stmt.setBoolean(2, mbio.isActive());
		    stmt.setInt(3, mbio.getOrderBy());
		    stmt.setInt(4, parent.getId());
		    stmt.executeUpdate();
		    try {
		    	ResultSet generatedKeys = stmt.getGeneratedKeys();
	            generatedKeys.next();
	            mbio.setId(generatedKeys.getInt(1));
	        }
		    catch(Exception e) {
		    	//System.out.println("Fel 1");
		    	e.printStackTrace();
		    }
		    conn.commit();
		} catch(Exception e) {
			e.printStackTrace();
		    return false;
		} finally {
		    try { if (stmt != null) stmt.close(); } catch (Exception e) {};
		    if(conn != null) { pool.releaseConnection(conn); }
		}
		parent.addItem(mbio);
		sortMBI(parent.getItems());
		return true;
	}
	
	public static MenuBarItemObject getMenuBarItemObject(MenuBarObject parent, String name) {
		JDBCConnectionPool pool = null;
		
		for(MenuBarItemObject mbio : parent.getItems()) {
			if(mbio.getVisual().equals(name)) {
				return mbio;
			}
		}
		
		return null;
	}


}
