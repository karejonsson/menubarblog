package se.prv.menubarblog.dbc;

import java.io.ByteArrayInputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import general.reuse.database.pool.JDBCConnectionPool;
import se.prv.menubarblog.domain.ImageObject;

public class ImagesDB {

	public static List<ImageObject> getAllImages() {
		List<ImageObject> out = new ArrayList<ImageObject>();
		 
		JDBCConnectionPool pool = null;
		
		Connection conn = null;
		
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
			pool = Commons.createPool();
		    conn = pool.reserveConnection();
		    stmt = conn.prepareStatement("select id, visual, symbol, orderby from images");
		    rs = stmt.executeQuery();
		    while(rs.next()) { 
		    	ImageObject io = new ImageObject();
		    	io.setId(rs.getInt(1));
		    	io.setVisual(rs.getString(2));
		    	io.setSymbol(rs.getString(3));
		    	io.setOrderBy(rs.getInt(4));
		    	out.add(io);
		    }
		} catch(Exception e) {
		    e.printStackTrace();
		} finally {
		    try { if (rs != null) rs.close(); } catch (Exception e) {};
		    try { if (stmt != null) stmt.close(); } catch (Exception e) {};
		    if(conn != null) { pool.releaseConnection(conn); }
		}
		sortIO(out);
		return out;
	}
	
	public static void sortIO(List<ImageObject> array) {
		Collections.sort(array, new Comparator<ImageObject>() {
		    @Override
		    public int compare(ImageObject o1, ImageObject o2) {
		    	try {
			        return o1.getOrderBy() - o2.getOrderBy();
		    	}
		    	catch(Exception e) {
		    		return 0;
		    	}
		    } }
		);
	}


	public static boolean deleteImageObject(ImageObject io) {
		JDBCConnectionPool pool = null;
		Connection conn = null;
		PreparedStatement stmt = null;
		try {
			pool = Commons.createPool();
		    conn = pool.reserveConnection();
		    stmt = conn.prepareStatement("delete from images where id = ?");
		    stmt.setInt(1, io.getId());
		    stmt.executeUpdate();
		    conn.commit();
		} catch(Exception e) {
			e.printStackTrace();
		    return false;
		} finally {
		    try { if (stmt != null) stmt.close(); } catch (Exception e) {};
		    if(conn != null) { pool.releaseConnection(conn); }
		}
		return true;
	}


	public static boolean updateImageObject(ImageObject io) {
		JDBCConnectionPool pool = null;
		Connection conn = null;
		PreparedStatement stmt = null;
		try {
			pool = Commons.createPool();
		    conn = pool.reserveConnection();
		    Integer orderBy = io.getOrderBy();
		    if(orderBy == null) {
			    stmt = conn.prepareStatement("update images set visual = ?, symbol = ? where id = ?");
		    }
		    else {
			    stmt = conn.prepareStatement("update images set visual = ?, symbol = ?, orderby = ? where id = ?");
		    }
		    stmt.setString(1, io.getVisual());
		    stmt.setString(2, io.getSymbol());
		    if(orderBy != null) {
			    stmt.setInt(3, io.getOrderBy());
		    }
		    stmt.setInt(3+((orderBy == null) ? 0 : 1), io.getId());
		    stmt.executeUpdate();
		    conn.commit();
		} catch(Exception e) {
			e.printStackTrace();
		    return false;
		} finally {
		    try { if (stmt != null) stmt.close(); } catch (Exception e) {};
		    if(conn != null) { pool.releaseConnection(conn); }
		}
		return true;
	}


	public static boolean createImageObject(ImageObject io) {
		JDBCConnectionPool pool = null;
		Connection conn = null;
		PreparedStatement stmt = null;
		try {
			pool = Commons.createPool();
		    conn = pool.reserveConnection();
		    Integer orderBy = io.getOrderBy();
		    if(orderBy == null) {
			    stmt = conn.prepareStatement("insert into images (visual, symbol) values ( ?, ? )", Statement.RETURN_GENERATED_KEYS);
		    }
		    else {
			    stmt = conn.prepareStatement("insert into images (visual, symbol, orderby) values ( ?, ?, ? )", Statement.RETURN_GENERATED_KEYS);
		    }
		    stmt.setString(1, io.getVisual());
		    stmt.setString(2, io.getSymbol());
		    if(orderBy != null) {
			    stmt.setInt(3, orderBy);
		    }
		    int affectedRows = stmt.executeUpdate();
		    //System.out.println("Skrivna rader "+affectedRows);
		    try {
		    	ResultSet generatedKeys = stmt.getGeneratedKeys();
	            generatedKeys.next();
	            io.setId(generatedKeys.getInt(1));
	        }
		    catch(Exception e) {
		    	//System.out.println("Fel 1");
		    	e.printStackTrace();
		    }
		    conn.commit();
		} catch(Exception e) {
	    	//System.out.println("Fel 2");
			e.printStackTrace();
		    return false;
		} finally {
		    try { if (stmt != null) stmt.close(); } catch (Exception e) {};
		    if(conn != null) { pool.releaseConnection(conn); }
		}
		return true;
	}

	public static ImageObject getImageObject(String symbol) {
		ImageObject out = null;
		JDBCConnectionPool pool = null;
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
		    pool = Commons.createPool();
		    conn = pool.reserveConnection();
		    stmt = conn.prepareStatement("select id, visual, orderby from images where symbol = ?");
		    stmt.setString(1, symbol);
		    rs = stmt.executeQuery();
		    while(rs.next()) {
		    	out = new ImageObject();
		    	out.setId(rs.getInt(1));
		    	out.setOrderBy(rs.getInt(2));
		    	out.setVisual(rs.getString(3));
		    	out.setSymbol(symbol);
		    }
		} catch(Exception e) {
		    e.printStackTrace();
		} finally {
		    try { if (rs != null) rs.close(); } catch (Exception e) {};
		    try { if (stmt != null) stmt.close(); } catch (Exception e) {};
		    if(conn != null) { pool.releaseConnection(conn); }
		}
		return out;
	}

	public static boolean setData(ImageObject io, byte[] bytes) {
		//System.out.println("ArticlesDB.setData "+new String(bytes)+", id="+ao.getId());
		JDBCConnectionPool pool = null;
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
		    pool = Commons.createPool();
		    conn = pool.reserveConnection();
		    stmt = conn.prepareStatement("update images set data = ? where id = ?");
		    //Blob blob = new Blob();
		    //blob.setBytes(0, bytes);
		    //stmt.setBytes(1,  bytes);
		    stmt.setBinaryStream(1, new ByteArrayInputStream(bytes != null ? bytes : new byte[0]), (bytes != null ? bytes.length : 0));
		    stmt.setInt(2, io.getId());
		    int count = stmt.executeUpdate();
		    conn.commit();
		    //System.out.println("ArticlesDB.setData, count="+count);
		} catch(Exception e) {
		    e.printStackTrace();
		    return false;
		} finally {
		    try { if (rs != null) rs.close(); } catch (Exception e) {};
		    try { if (stmt != null) stmt.close(); } catch (Exception e) {};
		    if(conn != null) { pool.releaseConnection(conn); }
		}
		return true;
	}

	public static byte[] getData(ImageObject io) {
		JDBCConnectionPool pool = null;
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
		    pool = Commons.createPool();
		    conn = pool.reserveConnection();
		    stmt = conn.prepareStatement("select data from images where id = ?");
		    stmt.setInt(1, io.getId());
		    rs = stmt.executeQuery();
		    rs.next();
		    //Blob blob = rs.getBlob(1); // creates the blob object from the result
		    /*
		       blob index starts with 1 instead of 0, and starting index must be (long).
		       we want all the bytes back, so this grabs everything.
		       keep in mind, if the blob is longer than max int length, this won't work right
		       because a byte array has max length of max int length.
		    */
		    //byte[] theBytes = blob.getBytes(1L, (int)blob.length());

		    return ArticlesDB.getByteArrayFromStream(rs.getBinaryStream(1));
		} catch(Exception e) {
		    e.printStackTrace();
		    return null;
		} finally {
		    try { if (rs != null) rs.close(); } catch (Exception e) {};
		    try { if (stmt != null) stmt.close(); } catch (Exception e) {};
		    if(conn != null) { pool.releaseConnection(conn); }
		}
	}
	
	public static byte[] getData(String symbol) {
		JDBCConnectionPool pool = null;
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
		    pool = Commons.createPool();
		    conn = pool.reserveConnection();
		    stmt = conn.prepareStatement("select data from images where symbol = ?");
		    stmt.setString(1, symbol);
		    rs = stmt.executeQuery();
		    rs.next();
		    return ArticlesDB.getByteArrayFromStream(rs.getBinaryStream(1));
		} catch(Exception e) {
		    e.printStackTrace();
		    return null;
		} finally {
		    try { if (rs != null) rs.close(); } catch (Exception e) {};
		    try { if (stmt != null) stmt.close(); } catch (Exception e) {};
		    if(conn != null) { pool.releaseConnection(conn); }
		}
	}
	
	public static ImageObject getImageObject(Integer articleId) {
		ImageObject out = null;
		JDBCConnectionPool pool = null;
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
		    pool = Commons.createPool();
		    conn = pool.reserveConnection();
		    stmt = conn.prepareStatement("select visual, symbol, orderby from images where id = ?");
		    stmt.setInt(1, articleId);
		    rs = stmt.executeQuery();
		    while(rs.next()) {
		    	out = new ImageObject();
		    	out.setId(articleId);
		    	out.setVisual(rs.getString(1));
		    	out.setSymbol(rs.getString(2));
		    	out.setOrderBy(rs.getInt(3));
		    }
		} catch(Exception e) {
		    e.printStackTrace();
		} finally {
		    try { if (rs != null) rs.close(); } catch (Exception e) {};
		    try { if (stmt != null) stmt.close(); } catch (Exception e) {};
		    if(conn != null) { pool.releaseConnection(conn); }
		}
		return out;
	}

}
