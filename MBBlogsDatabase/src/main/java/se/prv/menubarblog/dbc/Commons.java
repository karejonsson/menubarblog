package se.prv.menubarblog.dbc;

import general.reuse.database.pool.JDBCConnectionPool;
import general.reuse.database.pool.SimpleJDBCConnectionPool;
import se.prv.menubarblog.internal.InstallationProperties;

public class Commons {
  
	private static JDBCConnectionPool pool = null;

	public static JDBCConnectionPool createPool() throws Exception {
		if(pool != null) {
			return pool;
		}
		pool = new SimpleJDBCConnectionPool(
				InstallationProperties.getString(InstallationProperties.jdcb_driver_class_property, "org.postgresql.Driver"),
				InstallationProperties.getString(InstallationProperties.jdcb_url_property, "jdbc:postgresql://127.0.0.1:5432/mbblog"),
				InstallationProperties.getString(InstallationProperties.jdcb_username_property, "tunnelsup"),
				InstallationProperties.getString(InstallationProperties.jdcb_password_property, "abc123"));
		return pool;
	}

}