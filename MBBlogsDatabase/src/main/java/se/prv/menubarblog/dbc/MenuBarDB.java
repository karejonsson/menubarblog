package se.prv.menubarblog.dbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import general.reuse.database.pool.JDBCConnectionPool;
import se.prv.menubarblog.domain.MenuBarItemObject;
import se.prv.menubarblog.domain.MenuBarObject;

public class MenuBarDB {

	public static List<MenuBarObject> getMenuBar() {
		List<MenuBarObject> out = new ArrayList<MenuBarObject>();
		
		JDBCConnectionPool pool = null;
		
		Connection conn = null;
		
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
			pool = Commons.createPool();
		    conn = pool.reserveConnection();
		    stmt = conn.prepareStatement("select id, orderby, visual, active from menubar");
		    rs = stmt.executeQuery();
		    while(rs.next()) {
		    	MenuBarObject mbo = new MenuBarObject();
		    	mbo.setId(rs.getInt(1));
		    	mbo.setOrderBy(rs.getInt(2));
		    	mbo.setVisual(rs.getString(3));
		    	//System.out.println("Visual 1 <"+mbo.getVisual()+">");
		    	mbo.setActive(rs.getBoolean(4));
		    	out.add(mbo); 
		    }
		} catch(Exception e) {
		    // Error Handling
		} finally {
		    try { if (rs != null) rs.close(); } catch (Exception e) {};
		    try { if (stmt != null) stmt.close(); } catch (Exception e) {};
		    if(conn != null) { pool.releaseConnection(conn); }
		}
		
		sortMB(out);
		
		for(MenuBarObject mbo : out) {
			mbo.setItems(MenuBarItemDB.getMenuBarItems(mbo));
		}
		
		return out;
	}
	
	
	public static void sortMB(List<MenuBarObject> array) {
		Collections.sort(array, new Comparator<MenuBarObject>() {
		    @Override
		    public int compare(MenuBarObject o1, MenuBarObject o2) {
		    	try {
			        return o1.getOrderBy() - o2.getOrderBy();
		    	}
		    	catch(Exception e) {
		    		return 0;
		    	}
		    } }
		);		
	}

	public static boolean deleteMenuBarObject(List<MenuBarObject> menubar, MenuBarObject mbo) {
		
		JDBCConnectionPool pool = null;
		Connection conn = null;
		PreparedStatement stmt = null;
		try {
			pool = Commons.createPool();
		    conn = pool.reserveConnection();
		    stmt = conn.prepareStatement("delete from menubar where id = ?");
		    stmt.setInt(1, mbo.getId());
		    stmt.executeUpdate();
		    conn.commit();
		} catch(Exception e) {
			e.printStackTrace();
		    return false;
		} finally {
		    try { if (stmt != null) stmt.close(); } catch (Exception e) {};
		    if(conn != null) { pool.releaseConnection(conn); }
		}
		menubar.remove(mbo);
		sortMB(menubar);
		return true;
	}
	
	public static boolean updateMenuBarObject(List<MenuBarObject> menubar, MenuBarObject mbo) {
		
		JDBCConnectionPool pool = null;
		Connection conn = null;
		PreparedStatement stmt = null;
		try {
			pool = Commons.createPool();
		    conn = pool.reserveConnection();
		    stmt = conn.prepareStatement("update menubar set visual = ?, active = ?, orderBy = ? where id = ?");
		    stmt.setString(1, mbo.getVisual());
		    stmt.setBoolean(2, mbo.isActive());
		    stmt.setInt(3, mbo.getOrderBy());
		    stmt.setInt(4, mbo.getId());
		    stmt.executeUpdate();
		    conn.commit();
		} catch(Exception e) {
			e.printStackTrace();
		    return false;
		} finally {
		    try { if (stmt != null) stmt.close(); } catch (Exception e) {};
		    if(conn != null) { pool.releaseConnection(conn); }
		}
		sortMB(menubar);
		return true;
	}

	public static boolean createMenuBarObject(List<MenuBarObject> menubar, MenuBarObject mbo) {
		
		JDBCConnectionPool pool = null;
		Connection conn = null;
		PreparedStatement stmt = null;
		try {
			pool = Commons.createPool();
		    conn = pool.reserveConnection();
		    stmt = conn.prepareStatement("insert into menubar (visual, active) values ( ?, ? )", Statement.RETURN_GENERATED_KEYS);
		    stmt.setString(1, mbo.getVisual());
		    stmt.setBoolean(2, mbo.isActive());
		    stmt.executeUpdate();
		    try {
		    	ResultSet generatedKeys = stmt.getGeneratedKeys();
	            generatedKeys.next();
	            mbo.setId(generatedKeys.getInt(1));
	        }
		    catch(Exception e) {
		    	//System.out.println("Fel 1");
		    	e.printStackTrace();
		    }
		    conn.commit();
		} catch(Exception e) {
			e.printStackTrace();
		    return false;
		} finally {
		    try { if (stmt != null) stmt.close(); } catch (Exception e) {};
		    if(conn != null) { pool.releaseConnection(conn); }
		}
		if(menubar != null) {
			menubar.add(mbo);
			sortMB(menubar);
		}
		return true;
	}
	
	public static MenuBarObject getMenuBarObject(List<MenuBarObject> menus, String name) {
		JDBCConnectionPool pool = null;
		
		for(MenuBarObject mbo : menus) {
			if(mbo.getVisual().equals(name)) {
				return mbo;
			}
		}
		
		return null;
	}

	public static void correctChildRelations(List<MenuBarObject> menus) {
		Map<Integer, MenuBarObject> lookup = new HashMap<>();
		List<MenuBarItemObject> allItems = new ArrayList<>(); 
		for(MenuBarObject mbo : menus) {
			lookup.put(mbo.getId(), mbo);
			List<MenuBarItemObject> items = mbo.getItems();
			if(items != null && items.size() > 0) {
				allItems.addAll(mbo.getItems());
			}
			mbo.removeAllItem();
		}
		for(MenuBarItemObject mbio : allItems) {
			Integer parentId = mbio.getMenubarId();
			MenuBarObject mbo = lookup.get(parentId);
			if(mbo != null) {
				mbo.addItem(mbio);
			}
		}
		for(MenuBarObject mbo : menus) {
			MenuBarItemDB.sortMBI(mbo.getItems());
		}
	}

}
