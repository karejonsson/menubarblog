package se.prv.menubarblog.domain;

import java.io.Serializable;

public class AdminUser implements Serializable {
	   
	private int id;
	private String firstName = null;
	private String lastName = null;
	private String email = null;
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	private boolean active;
	private String password = null;
	private String userName = null;
			
	public String toString() {
		return "AdminUser: Användarnamn: "+userName+", Tilltalsnamn: "+firstName+", Efternamn: "+lastName;
	}
	
	public void setId(int id) {
		this.id = id;
	} 
	
	public int getId() {
		return id;
	}
	
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setActive(boolean active) {
		this.active = active;
	}
	
	public boolean isActive() {
		return active;
	}

	public void setUserName(String user) {
		this.userName = user;
	}
	
	public String getUserName() {
		return userName;
	}
	
	public void setPassword(String password) {
		this.password = password;
	}
	
	public String getPassword() {
		return password;
	}

}
