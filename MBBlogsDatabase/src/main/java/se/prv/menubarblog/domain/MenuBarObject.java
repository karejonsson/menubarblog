package se.prv.menubarblog.domain;

import java.io.Serializable;
import java.util.List;
import java.util.ArrayList;

public class MenuBarObject implements Serializable {
  
	private int id;
	private Integer orderBy = null;
	private String visual;
	private boolean active;
	private List<MenuBarItemObject> items = new ArrayList<MenuBarItemObject>();
	
	public String toString() {
		return "MenuBarObject: id="+id+", orderBy="+orderBy+", actvie="+active+", visual="+visual+", no items="+items.size();
	}

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public Integer getOrderBy() {
		return orderBy;
	}
	public void setOrderBy(Integer orderBy) {
		this.orderBy = orderBy;
	}
	public String getVisual() {
		return visual;
	}  
	public void setVisual(String visual) {
		this.visual = visual;
	}
	public boolean isActive() {
		return active;
	}
	public void setActive(boolean active) {
		this.active = active;
	}
	public List<MenuBarItemObject> getItems() {
		return items;
	}
	public void setItems(List<MenuBarItemObject> items) {
		this.items = items;
	}

	public void addItem(MenuBarItemObject mbio) {
		items.add(mbio);
	}
	
	public void removeItem(MenuBarItemObject mbio) {
		items.remove(mbio);
	}

	public void removeAllItem() {
		items.clear();
	}
	
}
