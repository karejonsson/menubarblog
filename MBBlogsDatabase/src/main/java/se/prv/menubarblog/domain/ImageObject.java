package se.prv.menubarblog.domain;

public class ImageObject {

	private int id;
	private String visual;
	private String symbol;
	private Integer orderBy;
 
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public String getVisual() {
		return visual;
	}
	
	public void setVisual(String visual) {
		this.visual = visual;
	}
	
	public String getSymbol() {
		return symbol;
	}
	
	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}
	
	public Integer getOrderBy() {
		return orderBy;
	}
	
	public void setOrderBy(Integer orderBy) {
		this.orderBy = orderBy;
	}

}
