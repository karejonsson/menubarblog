package se.prv.menubarblog.domain;

import java.io.Serializable;

public class ArticleObject implements Serializable {
	  
	private int id;
	private String visual = null;
	private Integer orderBy = null;
	
	public String toString() {
		return "Artikel: id="+id+", visual="+visual+", orderBy="+orderBy;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setVisual(String visual) {
		this.visual = visual;
	} 

	public String getVisual() {
		return visual;
	}

	public void setOrderBy(Integer orderBy) {
		this.orderBy = orderBy;
	}
	
	public Integer getOrderBy() {
		return orderBy;
	}

}
 