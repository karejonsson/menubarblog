 package se.prv.menubarblog.domain;

import java.io.Serializable;

public class MenuBarItemObject implements Serializable {
  
	private int id;
	private Integer orderBy;
	private boolean active;
	private int menubarId;
	private String visual;
	private Integer articleId;
	
	public String toString() {
		return "MenuBarItemObject: id="+id+", orderBy="+orderBy+", actvie="+active+", menubarId="+menubarId+", articleId="+articleId+", visual="+visual;
	}
		
	public void setId(int id) {
		this.id = id;
	}

	public int getId() {
		return id;
	} 

	public void setOrderBy(Integer orderBy) {
		this.orderBy = orderBy;
	}

	public Integer getOrderBy() {
		return orderBy;
	}

	public void setVisual(String visual) {
		this.visual = visual;
	}

	public String getVisual() {
		return visual;
	}
 
	public void setActive(boolean active) {
		this.active = active;
	}
	
	public boolean isActive() {
		return active;
	}

	public void setMenubarId(int menubarId) {
		this.menubarId = menubarId;
	}

	public int getMenubarId() {
		return menubarId;
	}

	public Integer getArticleId() {
		return articleId;
	}

	public void setArticleId(Integer articleId) {
		this.articleId = articleId;
	}

}
