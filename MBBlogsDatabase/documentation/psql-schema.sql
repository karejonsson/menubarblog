create table adminuser (
  id bigserial primary key,
  firstname varchar(50) not null,
  lastname varchar(100) not null,
  email varchar(100) not null,
  username varchar(30) not null,
  password varchar(60) not null,
  active bool default true
);

create table articles (
  id bigserial primary key,
  visual varchar(100) not null,
  orderby int default null
);

create table images (
  id bigserial primary key,
  visual varchar(100) not null,
  orderby int default null
  symbol varchar(100) not null,
  data bytea
);

create table menubar (
  id bigserial primary key,
  orderby int default null,
  visual varchar(100) not null,
  active bool default true
);

create table menubaritem (
  id bigserial primary key,
  orderby int default null,
  visual varchar(100) not null,
  active bool default true,
  menubarId bigint,
  articlesId bigint,
  foreign key ( menubarId ) references menubar (id),
  foreign key ( articlesId ) references articles (id)
);

